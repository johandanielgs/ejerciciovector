/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.Random;

/**
 * Esta clase modela un número entero más grande que un long
 * @author 1152358 - Israel Bulla Rey
 *         1152382 - Kevin Arias
 */
public class EnteroGrande {
    private int vector[];

    public EnteroGrande() {
    }

    public int[] getVector() {
        return vector;
    }

    public void setVector(int[] vector) {
        this.vector = vector;
    }
    
    public EnteroGrande(int n){
        if(n<=0){
            throw new RuntimeException("Error, no se pueden crear vectores con tamaño negativo o vacio");             
        }
        this.vector = new int[n];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = i;          
        }
    }
    
    public EnteroGrande(int inicio, int fin){
        if(inicio<=0 || fin<=0 || inicio>=fin){
            throw new RuntimeException("Error de índices");
        }
        this.vector = new int[fin - inicio + 1];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = crearNumero(inicio, fin);
        }
    }
    
    public EnteroGrande(int inicio, int fin, boolean esPar){
        if(inicio<=0 || fin<=0 || inicio>=fin){
            throw new RuntimeException("Error de índices");
        }
        if(esPar){
            if(inicio%2 != 0 && fin%2 != 0) this.vector = new int[(fin - inicio) / 2];
            else this.vector = new int[(fin - inicio) / 2 + 1];
        }
        else {
            if(inicio%2 == 0 && fin%2 == 0) this.vector = new int[(fin - inicio) / 2];
            else this.vector = new int[(fin - inicio) / 2 + 1];
        }
        for (int i = 0; i < vector.length; i++) {
                vector[i] = esPar ? crearNumeroPar(inicio, fin) : crearNumeroImpar(inicio, fin);
            }
    }
    
    public EnteroGrande getUnion(EnteroGrande n2){
        EnteroGrande union = new EnteroGrande();
        int tam, x = this.vector.length;
        if(this.getInterseccion(n2) == null) tam = this.vector.length + n2.getVector().length;
        else tam = this.vector.length + n2.getVector().length - this.getInterseccion(n2).getVector().length;
        int u[] = new int[tam];
        for (int i = 0; i < this.vector.length; i++) {
            u[i] = this.vector[i];
        }
        for (int i = 0; i < n2.getVector().length; i++) {
            if(!existe(n2.getVector()[i])){
                u[x] = n2.getVector()[i];
                x++;
            }
        }
        union.setVector(u);
        return union;
    }
    
    public EnteroGrande getInterseccion(EnteroGrande n2){
        EnteroGrande interseccion = new EnteroGrande();
        int tam = this.getElementosRepetidos(n2), x = 0;
        if(tam == 0) return null;
        int vecInter[] = new int[tam];
        for (int i = 0; i < n2.getVector().length; i++) {
            if(existe(n2.getVector()[i])){
                vecInter[x] = n2.getVector()[i];
                x++;
            }
        }
        interseccion.setVector(vecInter);
        return interseccion;
    }
    
    public EnteroGrande getDiferencia(EnteroGrande n2){
        EnteroGrande diferencia = new EnteroGrande();
        int tam, x = 0;
        if(this.getInterseccion(n2) == null) tam = this.vector.length;
        else tam = this.vector.length - this.getInterseccion(n2).getVector().length;
        if(tam == 0) return null;
        int vecDifer[] = new int[tam];
        for (int i = 0; i < this.vector.length; i++) {
            if(!n2.existe(this.getVector()[i])){
                vecDifer[x] = this.vector[i];
                x++;
            }
        }
        diferencia.setVector(vecDifer);
        return diferencia;
    }
    
    public int getElementosRepetidos(EnteroGrande n2){
        int tam = 0;
        for (int i = 0; i < n2.getVector().length; i++) {
                if(existe(n2.getVector()[i])) tam++;
        }
        return tam;
    }
    
    private int crearNumero(int inicio, int fin){
        int n;
        do{
            Random numero = new Random();
            n = numero.nextInt(inicio, fin + 1);
        }while(existe(n));
        return n;
    }
    
    private int crearNumeroPar(int inicio, int fin){
        int n;
        do{
            Random numero = new Random();
            n = numero.nextInt(inicio, fin + 1);
        }while(existe(n) || n%2!=0);
        return n;
    }

    private int crearNumeroImpar(int inicio, int fin){
        int n;
        do{
            Random numero = new Random();
            n = numero.nextInt(inicio, fin + 1);
        }while(existe(n) || n%2==0);
        return n;
    }
    
    public boolean existe(int x){
        if(this.esVacio()){
            throw new RuntimeException("Está vacío");
        }
        for (int i : vector) {
            if(i == x){
                return true;
            }
        }
        return false;
    }
    
    public boolean esVacio(){
        return this.vector == null;
    }
    
    @Override
    public String toString() 
{        String mensaje = "";
        for (int i : vector) {
            mensaje += i + "\t";
        }
        return mensaje;
    }
}
