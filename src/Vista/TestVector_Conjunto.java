/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.EnteroGrande;

/**
 * Main class
 * @authors 1152358 - Israel Bulla Rey.
 *          1152382 - Kevin David Arias Villamizar.
 */
public class TestVector_Conjunto {
    public static void main(String[] args) {
        EnteroGrande numero0 = new EnteroGrande(5);
        System.out.println("Numero 0: " + numero0.toString());
        
        EnteroGrande numero1 = new EnteroGrande(10);
        System.out.println("Numero 1: " + numero1.toString());
        
        EnteroGrande numero2 = new EnteroGrande(10, 20);
        System.out.println("Numero 2: " + numero2);
        
        EnteroGrande numero3 = new EnteroGrande(1, 24, false);
        System.out.println("Numero 3: " + numero3);
        
        EnteroGrande numero4 = new EnteroGrande(1, 24, true);
        System.out.println("Numero 4: " + numero4);
        
        EnteroGrande numero5 = new EnteroGrande(20, 30, true);
        System.out.println("Numero 5: " + numero5);
        
        EnteroGrande numero6 = new EnteroGrande(20, 30, false);
        System.out.println("Numero 6: " + numero6);
        
        EnteroGrande numero7 = new EnteroGrande(6, 12);
        System.out.println("Numero 7: " + numero7);
        
        //OPERACIONES
        
        /**
         * UNION
         */
        System.out.println("OPERACION DE UNION");
        System.out.println("Numero 3 con numero 0: " + numero3.getUnion(numero0));
        System.out.println("Numero 1 con numero 0: " + numero1.getUnion(numero0));
        System.out.println("Numero 5 con numero 6: " + numero5.getUnion(numero6));
        System.out.println("Numero 2 con numero 3: " + numero2.getUnion(numero3));
        System.out.println("Numero 4 con numero 6: " + numero4.getUnion(numero6));
        System.out.println("Numero 6 con numero 3: " + numero6.getUnion(numero3));
        
        /**
         * INTERSECCION
         * Las intersecciones vacias estan representadas con null
         */
        System.out.println("OPERACION DE INTERSECCION");
        System.out.println("Numero 0 con numero 3: " + numero0.getInterseccion(numero3));
        System.out.println("Numero 0 con numero 2: " + numero0.getInterseccion(numero2));
        System.out.println("Numero 2 con numero 3: " + numero2.getInterseccion(numero3));
        System.out.println("Numero 4 con numero 5: " + numero4.getInterseccion(numero5));
        System.out.println("Numero 0 con numero 6: " + numero0.getInterseccion(numero6));
        System.out.println("Numero 2 con numero 7: " + numero2.getInterseccion(numero7));
        
        /**
         * DIFERENCIA
         * Las diferencias vacias estan representadas con null
         */
        System.out.println("OPERACION DE DIFERENCIA");
        System.out.println("Numero 0 con numero 3: " + numero0.getDiferencia(numero3));
        System.out.println("Numero 3 con numero 0: " + numero3.getDiferencia(numero0));
        System.out.println("Numero 2 con numero 0: " + numero2.getDiferencia(numero0));
        System.out.println("Numero 0 con numero 1: " + numero0.getDiferencia(numero1));
        System.out.println("Numero 3 con numero 4: " + numero3.getDiferencia(numero4));
        System.out.println("Numero 2 con numero 7: " + numero2.getDiferencia(numero7));
    }
}
